
//ran out of time to implement this
export class Character{
    public character: Object = {
       
        id: 0,
        name: "",
        films: [],
        birth_year: "",
        eye_color: "",
        gender: "",
        hair_color: "",
        height: "",
        mass: "",
        skin_color: "",
        homeworld: "",
        species: [],
        starships: [],
        vehicles: [],
        url: "",
        created: "",
        edited: ""
    
};
}



export interface Character {
    id: number;
    name: string;
    films: any;
    birth_year: string;
    eye_color: string;
    gender: string;
    hair_color: string;
    height: string;
    mass: string; 
    skin_color: string; 
    homeworld: string;
    species: any; 
    starships: any; 
    vehicles: any;
    url: string; 
    created: string;
    edited: string;
}
