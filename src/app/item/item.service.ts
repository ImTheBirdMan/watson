import { Injectable } from "@angular/core";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { fromObject, fromObjectRecursive, Observable, PropertyChangeData } from "tns-core-modules/data/observable";
import { getFile, getImage, getJSON, getString, request, HttpResponse } from "tns-core-modules/http";

import { Character } from "./Character";

@Injectable({
    providedIn: "root"
})
export class ItemService {
    private serverUrl = "https://swapi.co/api/people/1";
    public http = require("http");
    public data: any; 
    public character: Character;
    public charArray = [];
    constructor() { }

    //This take the SWAPI call and uses the JSON to set up the Angular service passing- 
    //data to the view.
    getData(): any {
        getJSON("https://swapi.co/api/people").then((r: any) => {
            this.data = JSON.stringify(r);
            this.data = JSON.parse(this.data);
            for(let i = 0; i < this.data.results.length; i++){
                this.charArray.push(this.data.results[i].name);
            }
            }, (e) => {
        });
        return this.data;
    }

    //the name array that is used in place of a dropdown
    getChar(){
        return this.charArray; 
    }
  
}
