import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, EventEmitter } from "@angular/core";

import { Character } from "./Character";
import { ItemService } from "./item.service";
import { EventData } from "tns-core-modules/ui/page/page";
import { Observable, Subject } from 'rxjs';
// import { DropDownModule } from "nativescript-drop-down/angular";
// import { SelectedIndexChangedEventData } from "nativescript-drop-down";

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html",
    providers: [ItemService],
    changeDetection: ChangeDetectionStrategy.Default
})
export class ItemsComponent implements OnInit {
    selectedIndex = 1;
    choices: Array<string>;
    array: Array<Character>; 
    ddPressedbool = false; 
    character: Character = new Character(); 
    data: any; 
    hairColor: string;
    eyeColor: string;
    usesLightSaber: string; 

    
    @ViewChild('dd', {static: true}) dropDown: ElementRef;
    
    constructor(private myService: ItemService, private cdRef: ChangeDetectorRef) { }
    
    ngOnInit(): void {
       this.data = this.myService.getData();
       this.choices = [];
       setTimeout(()=>{
        this.array = this.myService.getChar(); 
        this.populateArray(); 

       }, 1000);
       this.cdRef.detectChanges(); 
    }

    /*
     FOLLOWING IS CODE FOR A DROPDOWN WHICH I REPLACED WITH A SCROLLVIEW
    */

    // public onchange(args: SelectedIndexChangedEventData) {
    //     console.log(`Drop Down selected index changed from ${args.oldIndex} to ${args.newIndex}`);
    //     console.log(this.dropDown.nativeElement.selectedIndex);
    // }
 
    // public onopen() {
    //     this.dropDown.nativeElement.selectedIndex
    //     console.log("Drop Down opened.");
    // }
 
    // public onclose() {
    //     console.log("Drop Down closed.");
    // }

    public populateArray(){
        //console.log(this.array);
       this.choices = this.myService.getChar(); 
       this.cdRef.detectChanges(); 
    }

    public showDetails(args){
       // console.log(eventData.object.get.name);
        console.log(args.object.text);
        console.log(args.object.get.name);
        for(let i = 0; i < this.myService.data.results.length; i++){
            if(args.object.text === this.myService.data.results[i].name){
                console.log('found it ' + this.myService.data.results[i].name);
                console.log(this.myService.data.results[i].hair_color);
                this.hairColor = "Hair Color: " + this.myService.data.results[i].hair_color;
                this.eyeColor = "Eye Color: " + this.myService.data.results[i].eye_color;
                if(this.myService.data.results[i].name === "Luke Skywalker" || 
                this.myService.data.results[i].name === "Darth Vader" ||
                this.myService.data.results[i].name == "Obi-Wan Kenobi"){
                    this.usesLightSaber = "Uses a lightsaber? The force is strong with this one.";
                }
                else{
                    this.usesLightSaber = "Uses a lightsaber? I find your lack of faith.....disturbing";
                }
            }
        }
    }


}
